declare module "@braintree/sanitize-url" {
  export function sanitizeUrl(url: string): string;
}

declare module "nativescript-zxing";

export const environment = {
  production: false,
  extension: false,
  docker: false,
  nativescript: true,
  VERSION: require("../../package.json").version
};

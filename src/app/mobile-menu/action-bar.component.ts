import { Component, Output, EventEmitter, Input } from "@angular/core";

@Component({
  selector: "ns-action-bar-component",
  moduleId: module.id,
  templateUrl: "./action-bar.component.html"
})
export class ActionBarComponent {
  @Input() title: string;
  @Output() menuTap = new EventEmitter();

  constructor() {}
}

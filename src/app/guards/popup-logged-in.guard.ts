import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Store } from "@ngrx/store";

import { SetIsPopup } from "../get-conf/conf.actions";
import { LoggedInGuard } from "./logged-in.guard";
import { IState } from "../app.reducers";

@Injectable()
export class PopupLoggedInGuard extends LoggedInGuard {
  constructor(public store: Store<IState>, public router: Router) {
    super(store, router);
  }
  /** This is a hook to store that the app is running in a popup.
   * It's actually very hard to detect if the app is running in the popup or not.
   */
  public canActivate() {
    this.store.dispatch(new SetIsPopup());
    return super.canActivate();
  }
}

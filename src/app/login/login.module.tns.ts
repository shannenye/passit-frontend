import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { EffectsModule } from "@ngrx/effects";
import { NgrxFormsModule } from "ngrx-forms";
import { StoreModule } from "@ngrx/store";

import * as fromLogin from "./login.reducer";
import { TNSLoginEffects } from "./tns-login-effects.tns";
import { LoginComponent } from "./login.component";
import { LoginContainer } from "./login.container";
import { SharedModule } from "../shared/shared.module";
import { ProgressIndicatorModule } from "../progress-indicator/progress-indicator.module";
import { VerifyMfaComponent } from "./verify-mfa/verify-mfa.component";
import { VerifyMfaContainer } from "./verify-mfa/verify-mfa.container";
import { LoginWrapperComponent } from "./login-wrapper/login-wrapper.component";
import { effects } from "./login.common";

@NgModule({
  declarations: [
    LoginComponent,
    LoginContainer,
    VerifyMfaComponent,
    VerifyMfaContainer,
    LoginWrapperComponent
  ],
  imports: [
    NativeScriptCommonModule,
    StoreModule.forFeature("login", fromLogin.reducers),
    EffectsModule.forFeature([...effects, TNSLoginEffects]),
    SharedModule,
    NgrxFormsModule,
    ProgressIndicatorModule
  ],
  exports: [LoginContainer],
  schemas: [NO_ERRORS_SCHEMA]
})
export class LoginModule {}

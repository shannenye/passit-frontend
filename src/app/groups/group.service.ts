import { Injectable } from "@angular/core";
import { from, throwError, Observable, forkJoin, of } from "rxjs";
import { catchError, mergeMap } from "rxjs/operators";

import { NgPassitSDK } from "../ngsdk/sdk";
import { IGroup, IGroupUser } from "../../passit_sdk/api.interfaces";
import { checkRespForErrors } from "../shared/utils";
import { IGroupContact } from "./interfaces";

@Injectable()
export class GroupService {
  constructor(public sdk: NgPassitSDK) {}

  /*
   * add group to secret using group id and secret id
   */
  public addGroupToSecret(groupId: number, secret: any) {
    return this.sdk
      .add_group_to_secret(groupId, secret.id)
      .then(resp => resp)
      .catch(err => console.error(err));
  }

  /*
   * create group
   */
  public create(name: string, currentMembers: any[], members: any[]) {
    // Add current user always!
    const userId = this.sdk.userId;
    if (currentMembers.indexOf(userId) === -1) {
      currentMembers.push(userId);
    }
    return from(this.sdk.create_group(name)).pipe(
      mergeMap(resp => {
        if (members.length > 1) {
          return this.updateGroupMembers(resp.id, members);
        }
        return of([]);
      }),
      catchError(err => {
        let errorMessage = "Unknown Error.";
        if (err.res) {
          const errors = checkRespForErrors(err.res);
          if (errors) {
            errorMessage = errors[0];
          }
        }
        return throwError(errorMessage);
      })
    );
  }

  public update(groupId: number, name: string, slug: string) {
    return from(this.sdk.update_group({ id: groupId, name, slug })).pipe(
      catchError(err => {
        let errorMessage = "Unknown Error.";
        if (err.res) {
          const errors = checkRespForErrors(err.res);
          if (errors) {
            errorMessage = errors[0];
          }
        }
        return throwError(errorMessage);
      })
    );
  }

  /*
   * get specific group using id
   */
  public getGroup(groupId: number): Promise<any> {
    return this.sdk
      .get_group(groupId)
      .then(resp => resp)
      .catch(err => console.error(err));
  }

  /**
   * Get groups from sdk
   * Returns Promise indicating result.
   */
  public getGroups() {
    return from(this.sdk.list_groups());
  }

  public removeGroupFromSecret(secret: any) {
    return this.sdk
      .get_secret(secret.id)
      .then(data => {
        return this.sdk
          .remove_group_from_secret(secret.id, data.secret_through_set[0].id!)
          .then(resp => {
            return resp;
          })
          .catch(err => console.error(err));
      })
      .catch(err => console.error(err));
  }

  public updateGroupMembers(groupId: number, groupMembers: IGroupContact[]) {
    return from(this.sdk.get_group(groupId)).pipe(
      mergeMap(group => {
        const existingMembers = [];
        for (const usergroup of group.groupuser_set) {
          existingMembers.push(usergroup.user);
        }
        const tasks: Array<Observable<IGroupUser | void>> = [];
        for (const member of groupMembers) {
          // If new member is not an existing member then add them
          if (existingMembers.indexOf(member.id) < 0) {
            tasks.push(
              from(
                this.sdk.add_user_to_group(group.id!, member.id, member.email)
              )
            );
          }
        }
        for (const member of existingMembers) {
          // If existing member is not in new group members then remove them
          if (
            groupMembers.map(groupMember => groupMember.id).indexOf(member) < 0
          ) {
            const memberGroupuser = group.groupuser_set.find(
              groupuser => groupuser.user === member
            );
            tasks.push(
              from(
                this.sdk.remove_user_from_group(group.id!, memberGroupuser!.id)
              )
            );
          }
        }
        if (tasks.length > 0) {
          return forkJoin(tasks);
        }
        return of([]);
      })
    );
  }

  public deleteGroup(groupId: number) {
    return from(this.sdk.delete_group(groupId));
  }

  acceptGroup(group: IGroup, userId: number) {
    const groupUser = this.findPendingGroupUser(group, userId);
    if (groupUser) {
      return from(this.sdk.acceptGroupInvite(groupUser.id));
    }
    throw Error("Unable to get groupUser for group accept");
  }

  declineGroup(group: IGroup, userId: number) {
    const groupUser = this.findPendingGroupUser(group, userId);
    if (groupUser) {
      return from(this.sdk.declineGroupInvite(groupUser.id));
    }
    throw Error("Unable to get groupUser for group decline");
  }

  private findPendingGroupUser(group: IGroup, userId: number) {
    return group.groupuser_set.find(
      groupuser => groupuser.user === userId && groupuser.is_invite_pending
    );
  }
}

import { ChangeDetectionStrategy, Component, OnInit } from "@angular/core";
import { Store, select } from "@ngrx/store";
import { loadGroups } from "../data/groups.actions";
import {
  selectPendingGroupsCount,
  selectActiveGroupsCount,
  selectActiveGroups,
  selectPendingGroups
} from "../data/group.selectors";
import {
  selectGroupsPageShowCreate,
  IAppState,
  selectGroupsPageGroupManaged
} from "./groups.reducer";
import {
  showCreate,
  setManaged,
  initGroups,
  acceptInvite,
  declineInvite
} from "./groups.actions";
import { IGroup } from "../data/interfaces";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <group-add-button (showCreate)="showCreate()"></group-add-button>
    <group-add [showCreate]="showCreate$ | async"></group-add>
    <app-group-empty-state
      [groupsCount]="groupsCount$ | async"
      [pendingGroupsCount]="pendingGroupsCount$ | async"
      [showCreate]="showCreate$ | async"
    ></app-group-empty-state>
    <app-group-pending-list
      [pendingInviteGroups]="pendingInviteGroups$ | async"
      (acceptGroup)="acceptGroup($event)"
      (declineGroup)="declineGroup($event)"
    ></app-group-pending-list>
    <group-list
      [groups]="groups$ | async"
      [groupManaged]="groupManaged$ | async"
      (groupSelected)="groupSelected($event)"
    ></group-list>
  `
})
export class GroupsContainer implements OnInit {
  groups$ = this.store.pipe(select(selectActiveGroups));
  groupsCount$ = this.store.pipe(select(selectActiveGroupsCount));
  pendingGroupsCount$ = this.store.pipe(select(selectPendingGroupsCount));
  pendingInviteGroups$ = this.store.pipe(select(selectPendingGroups));
  showCreate$ = this.store.pipe(select(selectGroupsPageShowCreate));
  groupManaged$ = this.store.pipe(select(selectGroupsPageGroupManaged));
  constructor(private store: Store<IAppState>) {}

  ngOnInit() {
    this.store.dispatch(initGroups());
  }

  /* update groups list state */
  getGroups() {
    this.store.dispatch(loadGroups());
  }

  showCreate() {
    this.store.dispatch(showCreate());
  }

  groupSelected(group: IGroup) {
    this.store.dispatch(setManaged(group));
  }

  acceptGroup(group: IGroup) {
    this.store.dispatch(acceptInvite({ group }));
  }

  declineGroup(group: IGroup) {
    this.store.dispatch(declineInvite({ group }));
  }
}

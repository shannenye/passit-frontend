import {
  HttpClientTestingModule,
  HttpTestingController
} from "@angular/common/http/testing";
import { TestBed } from "@angular/core/testing";

import { StoreModule } from "@ngrx/store";
import { Store } from "@ngrx/store";
import { LoginSuccessAction } from "../app.actions";
import { reducers } from "../account/account.reducer";
import * as fromRoot from "../app.reducers";
import { Api } from "./api";

describe("Testing sdk api: ", () => {
  let api: Api;
  let httpMock: HttpTestingController;
  let store: Store<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        StoreModule.forRoot(fromRoot.reducers),
        StoreModule.forFeature("account", reducers)
      ],
      providers: [Api]
    });

    api = TestBed.get(Api);
    httpMock = TestBed.get(HttpTestingController);
    store = TestBed.get(Store);
  });

  it("should be able to make GET requests", () => {
    const response = "hello";
    api.http.get("something").subscribe(data => {
      expect(data).toEqual(response);
    });
    const req = httpMock.expectOne("something");
    req.flush(response);
  });

  it("should be able to track the auth token from state", () => {
    expect(api.token).toBe("");
    const auth: any = {
      email: "test@example.com",
      publicKey: "",
      privateKey: "",
      userId: 1,
      userToken: "abc"
    };
    store.dispatch(new LoginSuccessAction(auth));
    expect(api.token).not.toBe("");
    auth.userToken = null; // Sort of fake log out
    store.dispatch(new LoginSuccessAction(auth));
    expect(api.token).toBe("");
  });
});

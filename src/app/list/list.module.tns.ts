import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NgrxFormsModule } from "ngrx-forms";
import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";

import { SharedModule } from "../shared/shared.module";
import { reducers } from "./list.reducer";
import { SecretFormEffects } from "./secret-form/secret-form.effects";
import { SecretListContainer } from "./list.container";
import { SecretListComponent } from "./list.component";
import { SecretDetailComponent } from "./detail.component";
import { SecretNewComponent } from "./new.component.tns";
import { ListActionBarComponent } from "./actionbar/action-bar.component";
import { ListActionBarContainer } from "./actionbar/action-bar.container";
import { SecretFormContainer } from "./secret-form/secret-form.container";
import { SecretFormComponent } from "./secret-form/secret-form.component";
import { DirectivesModule } from "../directives";
import { TNSSecretFormEffects } from "./secret-form/tns-secret-form.effects";

export const COMPONENTS = [
  SecretListContainer,
  SecretListComponent,
  SecretNewComponent,
  SecretFormContainer,
  SecretFormComponent,
  SecretDetailComponent,
  ListActionBarContainer,
  ListActionBarComponent
];

@NgModule({
  declarations: [COMPONENTS],
  imports: [
    NativeScriptCommonModule,
    NgrxFormsModule,
    SharedModule,
    DirectivesModule,
    StoreModule.forFeature("list", reducers),
    EffectsModule.forFeature([SecretFormEffects, TNSSecretFormEffects])
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class ListModule {}

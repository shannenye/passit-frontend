import {
  Component,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef
} from "@angular/core";
import { TextField } from "tns-core-modules/ui/text-field";

@Component({
  selector: "ns-list-action-bar-component",
  moduleId: module.id,
  templateUrl: "./action-bar.component.html"
})
export class ListActionBarComponent {
  @Input() showSearch: boolean;
  @Output() menuTap = new EventEmitter();
  @Output() searchTap = new EventEmitter();
  @Output() search = new EventEmitter<string>();
  @Output() clearSearch = new EventEmitter();

  @ViewChild("searchBar", { static: false }) searchBar: ElementRef;

  public onSearchChange(args: any) {
    const textField = <TextField>args.object;
    this.search.emit(textField.text);
  }

  public onClearSearchTap() {
    this.clearSearch.emit();
  }

  public onSearchTap() {
    this.searchTap.emit();
    setTimeout(() => {
      const searchElement: TextField = this.searchBar.nativeElement;
      searchElement.focus();
    }, 200);
  }

  constructor() {}
}

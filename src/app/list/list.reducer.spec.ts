import { ISecret } from "../../passit_sdk/api.interfaces";
import * as SecretActions from "../secrets/secret.actions";
import * as ListActions from "./list.actions";
import * as fromList from "./list.reducer";

describe("ListReducer", () => {
  describe("undefined action", () => {
    it("should return the default state", () => {
      const action = {} as any;

      const result = fromList.listReducer(undefined, action);
      expect(result).toEqual(fromList.initialListState);
    });
  });

  describe("CLEAR_MANAGED_SECRET", () => {
    it("should set secretManaged to null", () => {
      const createAction = new ListActions.ClearManagedSecret();
      const result = fromList.listReducer(
        fromList.initialListState,
        createAction
      );

      expect(result).toEqual(fromList.initialListState);
    });
  });

  describe("TOGGLE_MANAGED_SECRET", () => {
    it("should set managed secret", () => {
      const secretManaged = 1;

      const createAction = new ListActions.SetManagedSecret(secretManaged);
      const result = fromList.listReducer(
        fromList.initialListState,
        createAction
      );

      expect(result.secretManaged).toEqual(secretManaged);
    });
  });

  describe("SET_SEARCH_TEXT", () => {
    it("should set search text", () => {
      const searchText = "test";
      const createAction = new ListActions.SetSearchText(searchText);
      const result = fromList.listReducer(
        fromList.initialListState,
        createAction
      );

      expect(result.searchText).toEqual(searchText);
    });
  });

  describe("SHOW_CREATE", () => {
    it("should set showCreate to true", () => {
      const createAction = new ListActions.ShowCreate();
      const result = fromList.listReducer(
        fromList.initialListState,
        createAction
      );

      expect(result.showCreate).toEqual(true);
    });
  });

  describe("HIDE_CREATE", () => {
    it("should set showCreate to false", () => {
      const createAction = new ListActions.HideCreate();
      const result = fromList.listReducer(
        fromList.initialListState,
        createAction
      );

      expect(result).toEqual(fromList.initialListState);
    });
  });

  describe("REPLACE_SECRET_SUCCESS indicates a secret is finished saving.", () => {
    it("should set secretIsUpdating to false and secretIsUpdating to true", () => {
      const replacedSecret: ISecret = {
        name: "test",
        type: "test",
        id: 1,
        data: {},
        secret_through_set: []
      };
      const startState: fromList.IListState = {
        ...fromList.initialListState,
        secretManaged: 1
      };
      const createAction = new SecretActions.ReplaceSecretSuccessAction(
        replacedSecret
      );
      const result = fromList.listReducer(startState, createAction);

      // This should remain as we don't want to close a secret on save.
      expect(result.secretManaged).toEqual(1);
    });
  });
});

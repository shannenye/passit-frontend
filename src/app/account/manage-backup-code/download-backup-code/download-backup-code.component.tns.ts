import {
  Component,
  Input,
  ViewChild,
  ElementRef,
  AfterViewInit
} from "@angular/core";
import { Image } from "tns-core-modules/ui/image";
import { Printer } from "nativescript-printer";

const ZXing = require("nativescript-zxing");
const ImageSource = require("tns-core-modules/image-source");

@Component({
  selector: "app-download-backup-code",
  templateUrl: "./download-backup-code.component.html",
  styleUrls: [
    "./download-backup-code.component.scss",
    "../manage-backup-code.component.scss"
  ]
})
export class DownloadBackupCodeComponent implements AfterViewInit {
  @Input() code: string;
  @Input() styledCode: string;

  @ViewChild("barcodeImg", { static: false }) barcodeImg: ElementRef;
  @ViewChild("printArea", { static: false }) printArea: ElementRef;

  ngAfterViewInit() {
    this.generateBarcode();
  }

  styleBackupCode() {
    return this.code.replace(/(\w{4})/g, "$1 ").replace(/(^\s+|\s+$)/, " ");
  }

  generateBarcode() {
    if (this.code) {
      const barcodeImage = <Image>this.barcodeImg.nativeElement;
      const zx = new ZXing();
      const newImg = zx.createBarcode({
        encode: this.code,
        format: ZXing.QR_CODE
      });
      barcodeImage.imageSource = ImageSource.fromNativeSource(newImg);
    } else {
      alert("invalid code");
    }
  }

  print() {
    const printer = new Printer();
    printer.isSupported().then((supported: boolean) => {
      printer.printScreen({ view: this.printArea.nativeElement });
    });
  }

  getDate() {
    return new Date();
  }
}

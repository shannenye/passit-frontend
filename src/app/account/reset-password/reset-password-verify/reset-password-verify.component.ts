import {
  Component,
  Input,
  OnInit,
  EventEmitter,
  Output,
  ViewChild,
  ElementRef,
  OnDestroy
} from "@angular/core";
import { FormGroupState } from "ngrx-forms";
import jsQR from "jsqr";

import { IResetPasswordVerifyForm } from "./reset-password-verify.reducer";

@Component({
  selector: "app-reset-password-verify",
  templateUrl: "./reset-password-verify.component.html",
  styleUrls: [
    "./reset-password-verify.component.scss",
    "../../account.component.scss"
  ]
})
export class ResetPasswordVerifyComponent implements OnInit, OnDestroy {
  @Input()
  form: FormGroupState<IResetPasswordVerifyForm>;
  @Input()
  hasStarted: boolean;
  @Input()
  errorMessage: string;
  @Input()
  badCodeError: string | null;

  @Output()
  verify = new EventEmitter<string>();

  cameraError: string | null;
  showQuitButton = false;
  showScanner = false;
  video: HTMLVideoElement;
  canvas: CanvasRenderingContext2D;
  @ViewChild("canvas", { static: true })
  canvasRef: ElementRef<HTMLCanvasElement>;
  canvasElement: HTMLCanvasElement;
  localErrorMessage: string | null;
  videoBootstrapped = false;

  constructor() {}

  toggleScan() {
    this.showScanner = !this.showScanner;
    if (this.showScanner) {
      this.startScan();
    } else {
      // When there is no need to video, pause it and stop the event loop
      this.video.pause();
    }
  }

  startScan() {
    this.localErrorMessage = null;
    // Bootstrap if needed, otherwise start video loop again
    if (!this.videoBootstrapped) {
      navigator.mediaDevices
        .getUserMedia({
          video: { facingMode: "environment" }
        })
        .then(stream => {
          this.video.srcObject = stream;
          this.video.setAttribute("playsinline", "true"); // required to tell iOS safari we don't want fullscreen
          this.video.play();
          requestAnimationFrame(this.tick);
          this.videoBootstrapped = true;
        })
        .catch(err => {
          this.cameraError =
            "Unable to initialize the camera. Try typing the backup code in the text field below instead.";
          this.toggleScan();
        });
    } else {
      requestAnimationFrame(this.tick);
      this.video.play();
    }
  }

  stopStreamedVideo() {
    const stream = this.video.srcObject as MediaStream;
    if (stream) {
      const tracks = stream.getTracks();

      tracks.forEach(function(track: any) {
        track.stop();
      });

      this.video.srcObject = null;
    }
  }

  handleQrFound = (content: string) => {
    this.verify.emit(content);
  };

  onSubmit() {
    if (this.form.isValid) {
      this.verify.emit(this.form.value.code);
    }
  }

  ngOnInit() {
    this.video = document.createElement("video");
    this.canvasElement = this.canvasRef.nativeElement;
    this.canvas = this.canvasElement.getContext("2d")!;
  }

  ngOnDestroy() {
    this.stopStreamedVideo();
  }

  /*
   * Video event loop, runs forever while showScanner is true and then stops
   */
  tick = () => {
    if (this.showScanner) {
      if (this.video.readyState === this.video.HAVE_ENOUGH_DATA) {
        this.canvasElement.height = this.video.videoHeight;
        this.canvasElement.width = this.video.videoWidth;
        this.canvas.drawImage(
          this.video,
          0,
          0,
          this.canvasElement.width,
          this.canvasElement.height
        );
        const imageData = this.canvas.getImageData(
          0,
          0,
          this.canvasElement.width,
          this.canvasElement.height
        );
        const code = jsQR(imageData.data, imageData.width, imageData.height);
        this.showQuitButton = true;
        if (code) {
          this.handleQrFound(code.data);
        }
      }
      requestAnimationFrame(this.tick);
    }
  };
}

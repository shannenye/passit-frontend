import { Component } from "@angular/core";
import {
  Router,
  ActivatedRoute,
  NavigationEnd,
  ActivatedRouteSnapshot
} from "@angular/router";
import { Store, select } from "@ngrx/store";
import { LogoutAction } from "../../account/account.actions";
import { getIsLoggedIn } from "../../app.reducers";
import { getIsPrivateOrgMode, IState } from "../../app.reducers";
import { filter, map } from "rxjs/operators";
import { selectHasGroupInvites } from "../../data/group.selectors";

@Component({
  selector: "navbar-container",
  template: `
    <navbar
      [isLoggedIn]="isLoggedIn$ | async"
      [isPrivateOrgMode]="isPrivateOrgMode$ | async"
      [hasGroupInvites]="hasGroupInvites$ | async"
      (logout)="logout()"
      *ngIf="isVisible"
    ></navbar>
  `
})
export class NavbarContainer {
  isLoggedIn$ = this.store.pipe(select(getIsLoggedIn));
  isPrivateOrgMode$ = this.store.pipe(select(getIsPrivateOrgMode));
  hasGroupInvites$ = this.store.pipe(select(selectHasGroupInvites));
  isVisible = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<IState>
  ) {
    // Get all child route data (of course Angular makes this hard due to child routes)
    this.router.events
      .pipe(
        filter(event => event instanceof NavigationEnd),
        map(() => this.route.snapshot),
        map(_route => {
          while (_route.firstChild) {
            _route = _route.firstChild;
          }
          return _route;
        })
      )
      .subscribe((_route: ActivatedRouteSnapshot) => {
        if (_route.data["showNavBar"] !== undefined) {
          this.isVisible = _route.data.showNavBar;
        } else {
          this.isVisible = true; // Default to visible
        }
      });
  }

  logout() {
    this.store.dispatch(new LogoutAction());
  }
}

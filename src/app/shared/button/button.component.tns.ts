import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output
} from "@angular/core";

@Component({
  selector: "app-button",
  template: `
    <button
      class="button"
      [class.button-default]="!type || type === 'default'"
      [class.button-border]="type === 'border'"
      [class.button-text]="type === 'text'"
      [attr.width]="width ? width : null"
      [attr.height]="height ? height : null"
      [text]="text"
      [isEnabled]="isEnabled"
      (tap)="tap.emit()"
    ></button>
  `,
  styles: [
    `
      .button {
        font-size: 14;
        letter-spacing: 0.06;
      }
      .button-default {
        background-color: #6cc780;
        color: white;
      }
      .button-border {
        background-color: #ffffff;
        border-width: 1;
        border-color: #0092a8;
        border-radius: 3;
        color: #0092a8;
        padding: 0 10;
      }
      .button-text {
        background-color: #ffffff;
        border-width: 1;
        border-color: #ffffff;
        z-index: 0;
        color: #0092a8;
      }
    `
  ],
  moduleId: module.id,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonComponent {
  @Input() text: string;
  @Input() type: string;
  @Input() width: string;
  @Input() height: string;
  @Input() isEnabled = true;
  @Output() tap = new EventEmitter();

  constructor() {}
}

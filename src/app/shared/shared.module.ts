import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { InlineSVGModule } from "ng-inline-svg";
import { TooltipModule } from "ngx-tooltip";
import { NgrxFormsModule } from "ngrx-forms";

import { NavbarComponent } from "./navbar/navbar.component";
import { NavbarContainer } from "./navbar/navbar.container";
import { SearchComponent } from "./search";
import { CheckboxComponent } from "./checkbox/checkbox.component";
import { NonFieldMessagesComponent } from "./non-field-messages/non-field-messages.component";
import { FormLabelComponent } from "./form-label/form-label.component";
import { AsideLinkComponent } from "./aside-link/aside-link.component";
import { TextLinkComponent } from "./text-link/text-link.component";
import { TextFieldComponent } from "./text-field/text-field.component";
import { HeadingComponent } from "./heading/heading.component";
import { MultiselectComponent } from "./multiselect/multiselect.component";
import { MultiselectListComponent } from "./multiselect/multiselect-list/multiselect-list.component";
import { MultiselectAddComponent } from "./multiselect/multiselect-add/multiselect-add.component";
import { BadgeComponent } from "./multiselect/badge/badge.component";
import { MarketingFrameComponent } from "./marketing-frame/marketing-frame.component";
import { ServerSelectComponent } from "./server-select/server-select.component";
import { ProgressIndicatorModule } from "../progress-indicator/progress-indicator.module";

export const COMPONENTS = [
  NavbarContainer,
  NavbarComponent,
  SearchComponent,
  CheckboxComponent,
  NonFieldMessagesComponent,
  FormLabelComponent,
  AsideLinkComponent,
  TextFieldComponent,
  HeadingComponent,
  TextLinkComponent,
  MarketingFrameComponent,
  ServerSelectComponent,
  MultiselectComponent,
  MultiselectListComponent,
  MultiselectAddComponent,
  BadgeComponent
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    InlineSVGModule,
    TooltipModule,
    NgrxFormsModule,
    ProgressIndicatorModule
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS
})
export class SharedModule {}

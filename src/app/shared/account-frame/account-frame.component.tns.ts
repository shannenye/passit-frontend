import {
  Component,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ElementRef,
  OnChanges
} from "@angular/core";

@Component({
  selector: "account-frame",
  templateUrl: "./account-frame.component.html",
  styleUrls: ["./account-frame.component.css"]
})
export class AccountFrameComponent implements OnChanges {
  @Input() title?: string;
  @Input() titleButton?: string;
  @Input() submitText?: string;
  @Input() bottomLeftText?: string;
  @Input() nonFieldErrors?: string[];

  @Output() onSubmit = new EventEmitter();
  @Output() titleAction = new EventEmitter();
  @Output() bottomLeftAction = new EventEmitter();

  @ViewChild("ScrollList", { static: false }) scrollList: ElementRef;

  // scrolls to top when there are nonfield errors so the user can see the notification
  ngOnChanges() {
    if (this.nonFieldErrors) {
      this.scrollList.nativeElement.scrollToVerticalOffset(0);
    }
  }
}

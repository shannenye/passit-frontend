interface IFormFillRequestFields {
  fields: {
    login: string;
    secret: string;
  };
}

export interface IFormFillRequest {
  fields: string[];
  origin: string;
  allowForeign?: boolean;
  login: IFormFillRequestFields;
}

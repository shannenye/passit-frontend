#!/bin/bash
echo "Get access token for client id ${ANDROID_CLIENT_ID}"
ACCESS_TOKEN=$(curl "https://accounts.google.com/o/oauth2/token" -d "client_id=${ANDROID_CLIENT_ID}&client_secret=${ANDROID_CLIENT_SECRET}&refresh_token=${ANDROID_REFRESH_TOKEN}&grant_type=refresh_token&redirect_uri=urn:ietf:wg:oauth:2.0:oob" | jq -r .access_token)

echo "Create new edit for ${packageName}"
EDIT_ID=$(curl -H "Authorization: Bearer ${ANDROID_ACCESS_TOKEN}" -X POST https://www.googleapis.com/androidpublisher/v3/applications/${packageName}/edits | jq -r .id)

echo "Upload apk to Google Play for edit id ${EDIT_ID}"
curl -H "Authorization: Bearer ${ANDROID_ACCESS_TOKEN}" -X POST --header "Content-Type:application/vnd.android.package-archive" -T platforms/android/app/build/outputs/apk/release/app-release.apk -v "https://www.googleapis.com/upload/androidpublisher/v3/applications/${packageName}/edits/${EDIT_ID}/apks?uploadType=media"

echo "Submit apk to Beta Track for edit id ${EDIT_ID}"
curl -H "Authorization: Bearer ${ANDROID_ACCESS_TOKEN}" -X PUT -v "https://www.googleapis.com/androidpublisher/v3/applications/${packageName}/edits/${CI_PIPELINE_IID}/tracks/beta"

echo "Commit edit id ${EDIT_ID}"
curl -H "Authorization: Bearer ${ANDROID_ACCESS_TOKEN}" -X POST https://www.googleapis.com/androidpublisher/v3/applications/${packageName}/edits/${EDIT_ID}:commit

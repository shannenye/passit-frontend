import { browser, by, element, Key, ProtractorBrowser } from "protractor";

export class GroupsPage {
  navigateTo = async () => await browser.get("/groups");
  getNewGroupBtnElem = () => element(by.id("showAddGroupform"));

  // new form
  cancelFormByClick = async () =>
    await element(by.className("group-form__cancel-btn")).click();
  enterGroupName = async (name: string) =>
    await element(by.id("nameInput")).sendKeys(name);
  newGroup = async (testBrowser: ProtractorBrowser) =>
    await testBrowser.element(by.id("showAddGroupform")).click();

  // edit form
  deleteGroupByClick = async () =>
    await element(by.className("group-form__delete-btn")).click();
  editGroupName = async (name: string) => await this.enterGroupName(name);
  getManageGroupToggleBtnElem = () => element(by.id("manageSecretBtn"));

  // element selectors and actions
  getAllGroupsElem = () => element.all(by.css(".secret-list-item"));
  getActiveGroupFormCompElem = () => element(by.tagName("group-form"));
  getFormElem = () => element(by.className("secret__details"));
  getGroupTitleElem = () => element(by.css(".secret__heading .secret__title"));
  getNameFormInputElem = () => element(by.id("nameInput"));
  getNewlyCreatedGroupElem = () =>
    element.all(by.css(".secret-list-item")).last();
  saveButton = () => element(by.className("group-form__save-btn"));
  saveGroupByClick = async () =>
    await element(by.className("group-form__save-btn")).click();
  submitGroupByEnter = async () =>
    await element(by.id("nameInput")).sendKeys(Key.ENTER);

  membersInput = () => element(by.id("membersInput"));

  // specific member
  groupMember = (name: string) =>
    element(by.cssContainingText(".option", `${name}`));

  getMembersInput = () => element(by.css(".multiple input"));
  membersList = () => element(by.tagName("select-dropdown"));

  getDropDownItem = (name: string) =>
    element(by.xpath(`//span[contains(string(), "${name}")]`));
}
